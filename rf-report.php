<?php
// if Postback, get form post values, scrub input for bad stuff,
// else bounce us back to form at index.html
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $call    = test_input($_POST["call"]);
    $band    = test_input($_POST["band"]);
    $mode    = test_input($_POST["mode"]);
    $duty    = test_input($_POST["duty"]);
    $antenna = test_input($_POST["antenna"]);
    $height  = test_input($_POST["height"]);
    $gain    = test_input($_POST["gain"]);
    $power   = test_input($_POST["power"]);
    if ($_POST["ground"] == "true") {
        $ground = true;
    } else {
        $ground = false;
    }
} else {
    header('Location: index.html');
    exit;
}
// get center freq for each band
$freq       = 0.0;
$prettyBand = "";
switch ($band) {
    case 2200:
        $freq       = 0.1368;
        $prettyBand = "2200 Meters (135.7 - 137.8 kHz)";
        break;
    case 600:
        $freq       = 0.4755;
        $prettyBand = "600 Meters (472 - 479 kHz)";
        break;
    case 160:
        $freq       = 1.9;
        $prettyBand = "160 Meters (1800-2000 kHz)";
        break;
    case 80:
        $freq       = 3.75;
        $prettyBand = "80 Meters (3.5 - 4.0 MHz)";
        break;
    case 60:
        $freq       = 5.3685;
        $prettyBand = "60 Meters (5.3305 - 5.4065 MHz)";
        break;
    case 40:
        $freq       = 7.15;
        $prettyBand = "40 Meters (7.0 - 7.3 MHz)";
        break;
    case 30:
        $freq       = 10.125;
        $prettyBand = "30 Meters (10.1 - 10.15 MHz)";
        break;
    case 20:
        $freq       = 14.175;
        $prettyBand = "20 Meters (14.0 - 14.35 MHz";
        break;
    case 17:
        $freq       = 18.118;
        $prettyBand = "17 Meters (18.068 - 18.168 MHz)";
        break;
    case 15:
        $freq       = 21.225;
        $prettyBand = "15 Meters (21.0 - 21.45 MHz)";
        break;
    case 12:
        $freq       = 24.94;
        $prettyBand = "12 Meters (24.89 - 24.99 MHz)";
        break;
    case 10:
        $freq       = 28.85;
        $prettyBand = "10 Meters (28.0 - 29.7 MHz)";
        break;
    case 6:
        $freq       = 52.0;
        $prettyBand = "6 Meters (50.0 - 54.0 MHz)";
        break;
    case 2:
        $freq       = 146.0;
        $prettyBand = "2 Meters (144.0 - 148.0 MHz)";
        break;
    case 125:
        $freq       = 223.5;
        $prettyBand = "1.25 Meters (222.0 - 225.0 MHz)";
        break;
    case 70:
        $freq       = 435.0;
        $prettyBand = "70 Centimeters (420.0 - 450.0 MHz)";
        break;
    case 33:
        $freq       = 915.0;
        $prettyBand = "33 Centimeters (902.0 - 928.0 MHz)";
        break;
    case 23:
        $freq       = 1270.0;
        $prettyBand = "23 Centimeters (1240.0 - 1300.0 MHz)";
        break;
    case 13:
        $freq       = 2375.0;
        $prettyBand = "13 Centimeters (2.3 - 2.45 GHz)";
        break;
    case 9:
        $freq       = 3400.0;
        $prettyBand = "9 Centimeters (3.3 - 3.5 GHz)";
        break;
    case 5:
        $freq       = 5787.5;
        $prettyBand = "5 Centimeters (5.65 - 5.925 GHz)";
        break;
    case 3:
        $freq       = 10250.0;
        $prettyBand = "3 Centimeters (10.0 - 10.25 GHz)";
        break;
}

// if mode is other (-1) then we will use the entered duty value
// otherwise we take the value for mode and assign it to duty
if ($mode != '-1') {
    $duty = $mode;
}

// if antenna is other (-1) then we will use the entered gain value
// otherwise we take the value for antenna and assign it to gain
if ($antenna != '-1') {
    $gain = $antenna;
}

// our formula is wanting power in mW, we asked the user to use watts
// this is where we apply duty cycle to our power value as well
$pwr = ($power * ($duty / 100));

// calculate EIRP in milliwatts
$eirp = ($pwr * 1000) * pow(10.0, ($gain / 10.0));

// calculate distance based on freq
$CMaxDensity = 0.0;
$UMaxDensity = 0.0;
if ($freq < 1.34) {
    $CMaxDensity = 100.0;
    $UMaxDensity = 100.0;
} else if ($freq < 3.0) {
    $CMaxDensity = 100.0;
    $UMaxDensity = (180.0 / pow($freq, 2.0));
} else if ($freq < 30.0) {
    $CMaxDensity = (900.0 / pow($freq, 2.0));
    $UMaxDensity = (180.0 / pow($freq, 2.0));
} else if ($freq < 300.0) {
    $CMaxDensity = 1.0;
    $UMaxDensity = 0.2;
} else if ($freq < 1500.0) {
    $CMaxDensity = ($freq / 300.0);
    $UMaxDensity = ($freq / 1500.0);
} else if ($freq < 10000.0) {
    $CMaxDensity = 5.0;
    $UMaxDensity = 1.0;
}

// adjust for ground
if ($ground) {
    $gf      = 0.64;
    $gndCalc = "Calculated\n";
} else {
    $gf      = 0.25;
    $gndCalc = "Not Calculated\n";
}

// compliance distance for controlled "occupational" enviroment
$CMinDistance = sqrt(($gf * $eirp) / ($CMaxDensity * pi()));

// compliance distance for uncontrolled "non-occupational" environment
$UMinDistance = sqrt(($gf * $eirp) / ($UMaxDensity * pi()));
?>
<!DOCTYPE html>
<head>
    <title>RF Exposure Report for <?php echo $call; ?> by GridTracker.org</title>
</head>
<body>
    <h1>RF Exposure Report for <?php echo $call; ?></h1>
    <span>Report generated by GridTracker.org on <?php echo date(DATE_RFC2822); ?><br /></span>
    <div>
        <h3>Parameters</h3>
        RF Band: <?php echo $prettyBand; ?><br />
        Transmit Power: <?php echo $power; ?> watts<br />
        Duty Cycle: <?php echo $duty; ?>%<br />
        Average Power: <?php echo $pwr; ?> watts<br />
        Antenna Gain: <?php echo $gain; ?> dBi<br />
        EIRP: <?php echo round($eirp / 1000, 4); ?> watts<br />
        Antenna Height: <?php echo $height; ?> feet (<?php echo round($height / 3.281, 4); ?> meters)<br />
        Ground Reflections: <?php echo $gndCalc; ?>
    </div>
    <div>
        <h3>Controlled "Occupational" Environment:</h3>
        Maximum Allowed Power Density: <?php echo round($CMaxDensity, 4); ?> mw/cm<sup>2</sup><br />
        Minimum Safe Distance: <?php echo round($CMinDistance / 30.48, 4); ?> feet (<?php echo round($CMinDistance / 100, 4); ?> meters)<br />
        <?php if (($CMinDistance / 30.48) >= $height) {
    echo "Maximum Allowed Exposure at ground level exceeds the limits for Controlled \"Occupational\" Environment\n";
} else {
    echo "Maximum Allowed Exposure is " . round($height - ($CMinDistance / 30.48), 4) . " feet (" . round(($height / 3.281) - ($CMinDistance / 100), 4) . " meters) above ground for Controlled \"Occupational\" Environment\n";
}
?>
    </div>
    <div>
        <h3>Uncontrolled "Non-occupational" Environment:</h3>
        Maximum Allowed Power Density: <?php echo round($UMaxDensity, 4); ?> mw/cm<sup>2</sup><br />
        Minimum Safe Distance: <?php echo round($UMinDistance / 30.48, 4); ?> feet (<?php echo round($UMinDistance / 100, 4); ?> meters)<br />
        <?php if (($UMinDistance / 30.48) >= $height) {
    echo "Maximum Allowed Exposure at ground level exceeds the limits for Uncontrolled \"Non-occupational\" Environment\n";
} else {
    echo "Maximum Allowed Exposure is " . round($height - ($UMinDistance / 30.48), 4) . " feet (" . round(($height / 3.281) - ($UMinDistance / 100), 4) . " meters) above ground for Uncontrolled \"Non-occupational\" Environment\n";
}
?>
    </div>
    <div>
        <h3>Info</h3>
        Calcuated per FCC's OST Bulletin 65, originally issued in 1985, and revised in 1997<br />
        This report uses code derived from the public domain program written in BASIC by Wayne Overbeck N6NB in the January 1997 issue of CQ VHF, page 33
    </div>
</body>
</html>

<?php
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>