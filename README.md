# rf-exposure-calc

A php RF exposure calculator to assist amateurs in meeting new FCC Rules regarding the occupation and non-occupation RF exposure of their operations.